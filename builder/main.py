"""
    Build script for test.py
    test-builder.py
"""

from os.path import join
from SCons.Script import AlwaysBuild, Builder, Default, DefaultEnvironment

env = DefaultEnvironment()

#Temporary workaround for compiler paths being broken
platform = env.PioPlatform()
env['ENV']['COMPILER_PATH'] = '%s/lib/gcc/rl78-elf/7.2.0/' % (platform.get_package_dir("toolchain-gccrl78") or "")


# A full list with the available variables
# http://www.scons.org/doc/production/HTML/scons-user.html#app-variables
env.Replace(
    AR="rl78-elf-ar",
    AS="rl78-elf-as",
    CC="rl78-elf-gcc",
    CXX="rl78-elf-g++",
    OBJCOPY="rl78-elf-objcopy",
    RANLIB="rl78-elf-ranlib",
   
    #LDSCRIPT_PATH="rl78g10.ld",


)

if "BOARD" in env and env.BoardConfig().get("build.core") == "rl78g10":
    env.Append(
        ASFLAGS=[
            "-mg10"
        ],
        CCFLAGS=[   
            "-mg10",  # optimize for size
        ], 
        LINKFLAGS=[
            "-mg10"
        ])
    
    env.Replace(
        UPLOADER="rl78g10flash",
        UPLOADERFLAGS=[
            "-va",
            "/dev/ttyUSB0"
        ],
        UPLOADCMD='$UPLOADER $UPLOADERFLAGS $SOURCES 2k'
    )
  

env.Append(
    ASFLAGS=[
    ],
    CCFLAGS=[
        "-g",  # include debugging info (so errors include line numbers)
        "-Wall",  # show warnings
    ],
    
    LINKFLAGS=[
        "--specs=nosys.specs",
        "-nodefaultlibs",
        "-nostartfiles"
        
    ],
    
    
    BUILDERS=dict(
        ElfToBin=Builder(
            action=" ".join([
                "$OBJCOPY",
                "-O",
                "binary",
                "$SOURCES",
                "$TARGET"]),
            suffix=".bin"
        ),
        ElfToHex=Builder(
            action=env.VerboseAction(" ".join([
                "$OBJCOPY",
                "-O",
                "srec",
                "$SOURCES",
                "$TARGET"
            ]), "Building $TARGET"),
            suffix=".hex"
        )
    )
)


# The source code of "platformio-build-tool" is here
# https://github.com/platformio/platformio-core/blob/develop/platformio/builder/tools/platformio.py

#
# Target: Build executable and linkable firmware
#
target_elf = env.BuildProgram()

#
# Target: Build the .bin file
#
target_bin = env.ElfToBin(join("$BUILD_DIR", "firmware"), target_elf)

target_hex = env.ElfToHex(join("$BUILD_DIR", "firmware"), target_elf)
#
# Target: Upload firmware
#
upload = env.Alias(["upload"], target_hex, "$UPLOADCMD")
AlwaysBuild(upload)

#
# Target: Define targets
#
Default(target_hex)
